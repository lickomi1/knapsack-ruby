class KnapSackHeuristicAlg

  def initialize(instance)
    @instance = instance
  end
  
  def solve
    curLoad = 0
    curPrice = 0
    #sort items by price/weight ration
    @instance[:items].sort!{
      |x, y|
      x[:price]/x[:weight] <=> y[:price]/y[:weight]
    }
    #descendant order is needed
    @instance[:items].reverse!
   
    curWeight = 0   
    curPrice = 0
    index = 0
    
    #iterate while knapsack is not full
    while (curWeight < @instance[:weightLimit] && index < @instance[:items].size)
      if ((curWeight + @instance[:items][index][:weight]) < @instance[:weightLimit])
        curPrice += @instance[:items][index][:price]
        curWeight += @instance[:items][index][:weight]
      end
      index += 1
    end
    
    curPrice
  end
  
  def name?
    "HeuristicAlg"
  end
end