class KnapSackBruteForceAlg
  
  def initialize (instance)
    @instance = instance
  end
  
  def solve
    maxPrice = -1
    
    #generate all bit variations
    (0..2**@instance[:itemsCnt]).each{
      |x|
      currentConfig = getOccupationInfo? x
      weight = currentConfig[:weight]
      price = currentConfig[:price]
      if !isOverloaded?(weight) && (price > maxPrice)
        maxPrice = currentConfig[:price]
      end
      
    }

    maxPrice
  end
  
  def getOccupationInfo?(occupationVector)
    currentLoad = 0
    currentPrice = 0

    @instance[:items].each_with_index {
      |x, index|
      #check if item represented with bit of specified index presents in knapsuck
      presents = occupationVector & (2**index) > 0 ? 1 : 0

      #add current item to partial result if presented
      currentLoad += presents * x[:weight]
      currentPrice += presents * x[:price]
    }
    
    { :weight => currentLoad, :price => currentPrice }
  end
  
  def name?
    "BruteForceAlg"
  end
  
  def isOverloaded?(load)
    @instance[:weightLimit] < load
  end  
end