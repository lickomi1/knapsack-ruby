require "./Loader"
require "./KnapSackBruteForceAlg"
require "./KnapSackHeuristicAlg"
require "./KnapSackDynamicProgAlg"
require "./KnapSackBnB"
require "./KnapSackFPTAS"


def countTotals!(totalStats)
  totalStats.each{
    |x|
    x[:totalTime] = x[:totalTime]/x[:instanceCnt].to_f
    x[:totalDiff] = x[:totalDiff]/x[:instanceCnt].to_f
  }
end


files = ["./data/knap_4.inst.dat",
         "./data/knap_10.inst.dat",
         "./data/knap_15.inst.dat",
         "./data/knap_22.inst.dat",
         "./data/knap_25.inst.dat",
         "./data/knap_27.inst.dat",
         "./data/knap_30.inst.dat",
         "./data/knap_35.inst.dat"
        ]



#supported algorithms list
algorithms = [{:alg => KnapSackDynamicProgAlg, :params => []}, 
              {:alg => KnapSackFPTASAlg, :params => [5]},
              {:alg => KnapSackFPTASAlg, :params => [15]},
              {:alg => KnapSackFPTASAlg, :params => [30]},
              {:alg => KnapSackFPTASAlg, :params => [45]},
              {:alg => KnapSackFPTASAlg, :params => [60]}
            ]
files.each{
  |x|
  loader = Loader.new x
  puts "Solving for instances file #{x}"
  
  #initialize total stats for each alg
  totalStats = []
  algorithms.each{
    totalStats.push({:totalTime => 0, :totalDiff => 0, :instanceCnt => 0})
  }
  
  
  while (instance = loader.loadInstance)
    #solve problem instance by each of supported algorithms
    reference = nil
    instanceSolution = []
    algorithms.each_with_index{
      |x, index|
      start = Time.now
      solution = x[:alg].new(instance, *x[:params]).solve      
      time = Time.now - start

      result = {:time => time, :value => solution, :n => instance[:itemsCnt], :alg => index}
      reference ||= solution
      
      result[:diff] = (reference - solution)/reference.to_f
        
      #first alg is reference
      reference ||= solution
      totalStats[index][:totalTime] += time
      totalStats[index][:totalDiff] += result[:diff]
      totalStats[index][:instanceCnt] += 1  
      instanceSolution.push(result)
    }
    
    #print current instance solution
    puts(instanceSolution)
  end
  countTotals! totalStats
  puts totalStats
}


  
