require "./Parser"

class Loader
  
  def initialize(fileName)
    @fileName = fileName
    Dir.chdir(File.dirname(__FILE__))
    @file = File.new(fileName, "r")
  end
  
  #loads the line from an input file
  def loadInstance
    begin
      line = @file.gets
      line ? Parser.parseLine(line) : nil
    rescue 
      self.finalize
      puts "Could not read from file"
      raise
    end
  end

  def finalize
    @file.close
  end
end