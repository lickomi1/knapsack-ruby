class KnapSackBnBAlg
  
  def initialize (instance)
    @instance = instance
    
    #sort items by price/weight ration
    @instance[:items].sort!{
      |x, y|
      x[:price]/x[:weight] <=> y[:price]/y[:weight]
    }
    #descendant order is needed
    @instance[:items].reverse!
    @bestPrice = 0
  end
  
  def solve
    puts @instance
    recursiveSolve((0..@instance[:items].size-1).to_a, 0, 0)
    @bestPrice
  end
  
  
  
  def recursiveSolve(itemsToUse, curPrice, curWeight)
  #  puts itemsToUse
  #  puts "price:"
  #  puts curPrice
    
    if(@bestPrice < curPrice)
      @bestPrice = curPrice
    end
    #puts "--"

    if (curPrice + getBound(itemsToUse) < @bestPrice)
      return curPrice
    end
    
    res = curPrice
    itemsToUse.each_with_index do |x, index|
      if(canAdd(curWeight, x))
     #   puts "x: #{x}, #{@instance[:items][x][:price]}, curPrice: #{curPrice}"
        res = recursiveSolve(itemsToUse[index+1..itemsToUse.size-1], curPrice + @instance[:items][x][:price], curWeight + @instance[:items][x][:weight] )
      end
    end
    res
  end
  
  def canAdd(curWeight, x)
    @instance[:items][x][:weight] + curWeight <= @instance[:weightLimit]
  end
  
  def getBound(itemsToUse)
    sum = 0
    itemsToUse.each do |x|
      sum += @instance[:items][x][:price]
    end
    sum
  end

    
  
  def name?
    "BranchAndBoundAlg"
  end
  
  def isOverloaded?(load)
    @instance[:weightLimit] < load
  end  
end