class Parser
  
  #parses line line from input file and returns instance object
  def self.parseLine(line)
    lineParts = line.strip.split(" ")
    instanceObj = {
      :instanceId => Integer(lineParts[0]),    #first number should be unique ID
      :itemsCnt => Integer(lineParts[1]),      #represents number of items in a knapsack 
      :weightLimit => Integer(lineParts[2])    #max weight of items placed in the knapsack
    }
    
    items = []
    (3..lineParts.size - 1).step(2) do |x|
      items.push({
        :weight => Integer(lineParts[x]),
        :price => Integer(lineParts[x+1])
      })
    end
    
    instanceObj[:items] = items
    instanceObj
  end
  
end