class KnapSackDynamicProgAlg

  def initialize(instance)
    @instance = instance
    @table = Hash.new
    @table[[0, 0]] = 0
  end
  
  def solve 

    maxPrice = maxPrice?

    (0..maxPrice).each do |c|
      @table[[0, c]] ||= 999999
    end

    (0..@instance[:items].size).each do |i|
      @table[[i, 0]] = 0
    end

    (1..maxPrice).each do |c|
      (0..@instance[:items].size-1).each do |i|

    # puts "----"
    # puts "@instance[:items][#{i}][:price] = #{@instance[:items][i][:price]}"
    # puts "@instance[:items][#{i}][:weight] = #{@instance[:items][i][:weight]}"
    # puts "@table[[#{i}, #{c}] = #{@table[[i, c]]}"
      if (@instance[:items][i][:price] <= c)
        p = c - @instance[:items][i][:price];
        # puts "p: #{p}"
        if (p < 0)
          p = 0 
        end  
        @table[[i+1, c]] = [@table[[i, c]], @table[[i, p]] + @instance[:items][i][:weight]].min
        # puts "@table[[#{i+1}, #{c}]]: #{@table[[i+1, c]]}"
      else
        @table[[i+1, c]] = @table[[i, c]]
      end
        
      end
        # puts "last: #{@table[[@instance[:items].size-1, c]]}"
    end
    bestPrice? maxPrice
  end
  
  def bestPrice?(maxPrice)
    puts "#{@table[[]]}"
    (maxPrice).downto(0).each do |price|
      if (@table[[@instance[:items].size, price]] <= @instance[:weightLimit])
        return price
      end
    end
  end

  def maxPrice?

    sum = 0
    @instance[:items].each do |x|
      sum += x[:price]
    end
    sum
  end
  
  def name?
    "DynamicProgramingAlg"
  end
end