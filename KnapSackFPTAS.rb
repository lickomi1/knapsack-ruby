class KnapSackFPTASAlg

  def initialize(instance, maxError)
    @instance = instance
    @table = Hash.new
    @table[[0, 0]] = 0
    @maxError = maxError
  end


  def countExpandableBits(perc)
    relativeError = perc/100.0
    b = Math.log2((relativeError*maximalItemPrice)/@instance[:items].size).floor
    if(b < 0.0)
      return 0
    end
    b
  end

  def maximalItemPrice
    @instance[:items].collect{ |item| item[:price] }.max
  end

  def reducePrices(bits)
    @reducedPrices = []
    @instance[:items].each_with_index do |x, index|
      #puts "#{x[:price]} #{x[:price].to_s(2)} #{x[:price] >> bits} #{(x[:price] >> bits).to_s(2)}"
      @reducedPrices[index] = x[:price] >> bits
    end
  end
  
  def solve 
    bits = countExpandableBits(@maxError)
    reducePrices(bits)
    #puts "#{@reducedPrices}"
    
    maxPrice = @reducedPrices.inject(:+)


    (0..maxPrice).each do |c|
      @table[[0, c]] ||= 999999
    end

    (0..@instance[:items].size).each do |i|
      @table[[i, 0]] = 0
    end

    (1..maxPrice).each do |c|
      (0..@instance[:items].size-1).each do |i|

    # puts "----"
    # puts "@instance[:items][#{i}][:price] = #{@instance[:items][i][:price]}"
    # puts "@instance[:items][#{i}][:weight] = #{@instance[:items][i][:weight]}"
    # puts "@table[[#{i}, #{c}] = #{@table[[i, c]]}"
      if (@reducedPrices[i] <= c)
        @table[[i+1, c]] = [@table[[i, c]], @table[[i, c - @reducedPrices[i]]] + @instance[:items][i][:weight]].min
        # puts "@table[[#{i+1}, #{c}]]: #{@table[[i+1, c]]}"
      else
        @table[[i+1, c]] = @table[[i, c]]
      end
        
      end
        # puts "last: #{@table[[@instance[:items].size-1, c]]}"
    end
    bestPrice? maxPrice
  end
  
  def bestPrice?(maxPrice)
    #puts "#{@table[[]]}"
    (maxPrice).downto(0).each do |price|
      if (@table[[@instance[:items].size, price]] <= @instance[:weightLimit])
        #puts price
        return resolveRealPrice(price)
      end
    end
  end

  def resolveRealPrice(price)
    curPrice = price
    rPrice = 0
    #puts "resolving real price for #{price}"
    (@instance[:items].size).downto(1) do |i|
      #puts "item @table[[#{i}, #{curPrice}]] - #{@table[[i, curPrice]]}"
      if(@table[[i, curPrice]] != @table[[i-1, curPrice]])
        rPrice += @instance[:items][i-1][:price]
        curPrice -= @reducedPrices[i-1]
      end
      if (curPrice < 0)
        break
      end
    end
    rPrice
  end

  def maxPrice?

    sum = 0
    @instance[:items].each do |x|
      sum += x[:price]
    end
    sum
  end
  
  def name?
    "DynamicFPTASAlg"
  end
end